package com.epam.fibonacci.task.view;

@FunctionalInterface
public interface Printable {

    void print();

}
