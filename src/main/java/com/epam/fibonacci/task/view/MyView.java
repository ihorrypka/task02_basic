package com.epam.fibonacci.task.view;

import com.epam.fibonacci.task.controller.Controller;
import com.epam.fibonacci.task.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
/**
 * @author Ihor Rypka
 * @version 1.0
 * @since 08.10.2019
 */
public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    /**
     * To read the number from the console
     */
    private static Scanner input = new Scanner(System.in);

    /**
     * This is a constructor of MyView class.
     */
    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print odd and even numbers");
        menu.put("2", "  2 - print sum of odd and even numbers");
        menu.put("3", "  3 - print Fibonacci numbers");
        menu.put("4", "  4 - print percentage of odd and even Fibonacci numbers");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    /**
     * This method prints odd, even and reverse order even numbers
     */
    private void pressButton1() {
        System.out.println("Please input array interval [X;Y]");
        System.out.println("Please input the first number X: ");
        int firstNumber = input.nextInt();
        System.out.println("Please input the last number Y: ");
        int lastNumber = input.nextInt();
        System.out.println(controller.getNumberList(firstNumber, lastNumber));
        System.out.println("List of odd numbers: ");
        System.out.println(controller.getOddNumbers());
        System.out.println("List of even numbers: ");
        System.out.println(controller.getEvenNumbers());
        System.out.println("List of reverse even numbers: ");
        System.out.println(controller.getReverseEvenNumbers());
    }

    /**
     * This method prints the sum odd and even numbers
     */
    private void pressButton2() {
        System.out.println("Please input array interval [X;Y]");
        System.out.println("Please input the first number X: ");
        int firstNumber = input.nextInt();
        System.out.println("Please input the last number Y: ");
        int lastNumber = input.nextInt();
        System.out.println(controller.getNumberList(firstNumber, lastNumber));
        System.out.println(controller.getOddNumbers());
        System.out.println(controller.getEvenNumbers());
        System.out.println("Sum of odd numbers: ");
        System.out.println(controller.getSumOfOddNumbers());
        System.out.println("Sum of even numbers: ");
        System.out.println(controller.getSumOfEvenNumbers());
    }

    /**
     * This method print Fibonacci numbers with a given number of items
     */
    private void pressButton3() {
        System.out.println("Please input array interval [X;Y]");
        System.out.println("Please input the first number X: ");
        int firstNumber = input.nextInt();
        System.out.println("Please input the last number Y: ");
        int lastNumber = input.nextInt();
        System.out.println(controller.getNumberList(firstNumber, lastNumber));
        System.out.println(controller.getOddNumbers());
        System.out.println(controller.getEvenNumbers());
        System.out.println("Please input the amount of Fibonacci numbers: ");
        int amount = input.nextInt();
        System.out.println("List of fibonacci numbers: ");
        System.out.println(controller.getFibonacciNumber(amount));
    }

    /**
     * This method print percentage of odd and even Fibonacci numbers
     */
    private void pressButton4() {
        System.out.println("Please input array interval [X;Y]");
        System.out.println("Please input the first number X: ");
        int firstNumber = input.nextInt();
        System.out.println("Please input the last number Y: ");
        int lastNumber = input.nextInt();
        System.out.println(controller.getNumberList(firstNumber, lastNumber));
        System.out.println(controller.getOddNumbers());
        System.out.println(controller.getEvenNumbers());
        System.out.println("Please input the amount of Fibonacci numbers: ");
        int amount = input.nextInt();
        System.out.println("List of fibonacci numbers: ");
        System.out.println(controller.getFibonacciNumber(amount));
        System.out.println("The percentage of odd Fibonacci Numbers: ");
        System.out.println(controller.getPercentageOfOddFibonacciNumbers() + "%");
        System.out.println("The percentage of even Fibonacci Numbers: ");
        System.out.println(controller.getPercentageOfEvenFibonacciNumbers() + "%");
    }

    /**
     * This method prints all available functions
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * This method allows you to select functions
     */
    public void show() {
        String keyMenu = null;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getMessage();
            }
        } while (!keyMenu.equals("Q"));
    }
}