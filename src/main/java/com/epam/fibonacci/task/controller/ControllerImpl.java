package com.epam.fibonacci.task.controller;

import com.epam.fibonacci.task.model.BusinessLogic;
import com.epam.fibonacci.task.model.Model;

import java.util.List;

/**
 * @author Ihor Rypka
 * @version 1.0
 * @since 08.10.2019
 */
public class ControllerImpl implements Controller {
    private Model model;

    /**
     * This is a constructor of ControllerImpl class.
     */
    public ControllerImpl() {
        model = new BusinessLogic();
    }

    /**
     * This method is used to get the sequence of numbers
     * in an interval [firstNumber, lastNumber]
     *
     * @param firstNumber  from this parameter begins the interval
     *                       of numbers
     * @param lastNumber this parameter completes the interval
     *                       of numbers
     *
     * @return list of numbers
     */
    @Override
    public List<Integer> getNumberList(int firstNumber, int lastNumber) {
        return model.getNumberList(firstNumber, lastNumber);
    }

    /**
     * @return list of odd numbers
     */
    @Override
    public List<Integer> getOddNumbers() {
        return model.getOddNumbers();
    }

    /**
     * @return list of even numbers
     */
    @Override
    public List<Integer> getEvenNumbers() {
        return model.getEvenNumbers();
    }

    /**
     * @return list of reverse order even numbers
     */
    @Override
    public List<Integer> getReverseEvenNumbers() {
        return model.getReverseEvenNumbers();
    }

    /**
     * @return sum of odd numbers
     */
    @Override
    public int getSumOfOddNumbers() {
        return model.getSumOfOddNumbers();
    }

    /**
     * @return sum of even numbers
     */
    @Override
    public int getSumOfEvenNumbers() {
        return model.getSumOfEvenNumbers();
    }

    /**
     * This method is used to get Fibonacci numbers
     * in an interval with value of numbers[amount]
     *
     * @param amount this parameter is the value
     *              of Fibonacci numbers
     *
     * @return list of Fibonacci numbers
     */
    @Override
    public List<Integer> getFibonacciNumber(int amount) {
        return model.getFibonacciNumber(amount);
    }

    /**
     * This method calculates the percentage of
     * the occurrence of odd numbers
     *
     * @return a percentage of odd Fibonacci numbers
     */
    @Override
    public int getPercentageOfOddFibonacciNumbers() {
        return model.getPercentageOfOddFibonacciNumbers();
    }

    /**
     * This method calculates the percentage of
     * the occurrence of even numbers
     *
     * @return a percentage of even Fibonacci numbers
     */
    @Override
    public int getPercentageOfEvenFibonacciNumbers() {
        return model.getPercentageOfEvenFibonacciNumbers();
    }
}
