package com.epam.fibonacci.task.controller;

import java.util.List;

public interface Controller {
    List<Integer> getNumberList(int firstNumber, int lastNumber);
    List<Integer> getOddNumbers();
    List<Integer> getEvenNumbers();
    List<Integer> getReverseEvenNumbers();
    int getSumOfOddNumbers();
    int getSumOfEvenNumbers();
    List<Integer> getFibonacciNumber(int amount);
    int getPercentageOfOddFibonacciNumbers();
    int getPercentageOfEvenFibonacciNumbers();
}
