package com.epam.fibonacci.task.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @author Ihor Rypka
 * @version 1.0
 * @since 08.10.2019
 */
public class Domain {
    private List<Integer> numberList;
    private List<Integer> oddNumbers;
    private List<Integer> evenNumbers;
    private List<Integer> listOfFibonacciNumbers;
    private final int ONE_HUNDRED_PERCENT = 100;

    /**
     * This is a no argument constructor of Domain class.
     */
    public Domain() {

    }

    /**
     * This method is used to get the sequence of numbers
     * in an interval [firstNumber, lastNumber]
     *
     * @param firstNumber  from this parameter begins the interval
     *                       of numbers
     * @param lastNumber this parameter completes the interval
     *                       of numbers
     *
     * @return list of numbers
     */
    public List<Integer> getNumberList(int firstNumber, int lastNumber) {
        numberList = Domain.generateNumberList(firstNumber, lastNumber);
        return numberList;
    }

    /**
     * @return list of odd numbers
     */
    public List<Integer> getOddNumberList() {
        oddNumbers = Domain.printOddNumbers(numberList);
        return oddNumbers;
    }

    /**
     * @return list of even numbers
     */
    public List<Integer> getEvenNumberList() {
        evenNumbers = Domain.printEvenNumbers(numberList);
        return evenNumbers;
    }

    /**
     * @return list of reverse order even numbers
     */
    public List<Integer> getReverseEvenNumbers() {
        Domain.reverseEvenNumbers(evenNumbers);
        return evenNumbers;
    }

    /**
     * @return sum of odd numbers
     */
    public int getSumOfOddNumbers() {
        int sumOfOddNumbers = Domain.sumOfNumbers(oddNumbers);
        return sumOfOddNumbers;
    }

    /**
     * @return sum of even numbers
     */
    public int getSumOfEvenNumbers() {
        int sumOfEvenNumbers = Domain.sumOfNumbers(evenNumbers);
        return sumOfEvenNumbers;
    }

    /**
     * This method is used to get Fibonacci numbers
     * in an interval with value of numbers[amount]
     *
     * @param amount this parameter is the value
     *              of Fibonacci numbers
     *
     * @return list of Fibonacci numbers
     */
    public List<Integer> getFibonacciNumber(int amount) {
        int firstNumber = Collections.max(oddNumbers);
        int secondNumber = Collections.max(evenNumbers);
        int sumOfTwoNumbers = 0;
        List<Integer> fibonacciNumbers = new LinkedList<>();

        fibonacciNumbers.add(firstNumber);
        fibonacciNumbers.add(secondNumber);

        for(int i = 2; i < amount; i++) {
            sumOfTwoNumbers = firstNumber + secondNumber;
            fibonacciNumbers.add(sumOfTwoNumbers);
            firstNumber = secondNumber;
            secondNumber = sumOfTwoNumbers;
        }
        listOfFibonacciNumbers = fibonacciNumbers;
        return listOfFibonacciNumbers;
    }

    /**
     * This method calculates the percentage of
     * the occurrence of odd numbers
     *
     * @return a percentage of odd Fibonacci numbers
     */
    public int getPercentageOfOddFibonacciNumbers() {
        List<Integer> oddFibonacciNumber = Domain.printOddNumbers(listOfFibonacciNumbers);
        int fibonacciListSize = listOfFibonacciNumbers.size();
        int oddFibonacciListSize = oddFibonacciNumber.size();
        int oddPercentage = (int)((double)oddFibonacciListSize /
                                fibonacciListSize * ONE_HUNDRED_PERCENT);
        return oddPercentage;
    }

    /**
     * This method calculates the percentage of
     * the occurrence of even numbers
     *
     * @return a percentage of even Fibonacci numbers
     */
    public int getPercentageOfEvenFibonacciNumbers() {
        List<Integer> evenFibonacciNumber = Domain.printEvenNumbers(listOfFibonacciNumbers);
        int fibonacciListSize = listOfFibonacciNumbers.size();
        int evenFibonacciListSize = evenFibonacciNumber.size();
        int evenPercentage = (int) ((double)evenFibonacciListSize /
                                fibonacciListSize * ONE_HUNDRED_PERCENT);
        return evenPercentage;
    }

    /**
     * This method calculates the sum of numbers
     *
     * @return sum of numbers
     */
    private static int sumOfNumbers(List<Integer> list) {
        int oddSum = list.stream()
                .mapToInt(Integer::intValue)
                .sum();
        return oddSum;
    }

    /**
     * This method rebuilds the odd Fibonacci numbers in reverse order
     */
    private static void reverseEvenNumbers(List<Integer> list) {
        Collections.reverse(list);
    }

    /**
     * This metod generate the list of numbers
     *
     * @return list of Fibonacci numbers
     */
    private static List<Integer> generateNumberList(int firstNumber, int lastNumber) {
        List<Integer> list = new LinkedList<>();
        for (int i = firstNumber; i <= lastNumber; i++) {
            list.add(i);
        }
        return list;
    }

    /**
     * This metod print the list of odd numbers
     *
     * @return list of odd numbers
     */
    private static List<Integer> printOddNumbers(List<Integer> numberList) {
        List<Integer> oddListValues = numberList.stream()
                .filter(i -> i%2 != 0)
                .collect(Collectors.toList());
        return oddListValues;
    }

    /**
     * This metod print the list of even numbers
     *
     * @return list of even numbers
     */
    private static List<Integer> printEvenNumbers(List<Integer> numberList) {
        List<Integer> evenListValues = numberList.stream()
                .filter(i -> i%2 == 0)
                .collect(Collectors.toList());
        return evenListValues;
    }
}