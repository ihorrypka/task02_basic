package com.epam.fibonacci.task.model;

import java.util.List;

/**
 * @author Ihor Rypka
 * @version 1.0
 * @since 08.10.2019
 */
public class BusinessLogic implements Model {

    private Domain domain;

    /**
     * This is a constructor of BusinessLogic class.
     */
    public BusinessLogic() {
        domain = new Domain();
    }

    /**
     * This method is used to get the sequence of numbers
     * in an interval [firstNumber, lastNumber]
     *
     * @param firstNumber  from this parameter begins the interval
     *                       of numbers
     * @param lastNumber this parameter completes the interval
     *                       of numbers
     *
     * @return list of numbers
     */
    @Override
    public List<Integer> getNumberList(int firstNumber, int lastNumber) {
        return domain.getNumberList(firstNumber, lastNumber);
    }

    /**
     * @return list of odd numbers
     */
    @Override
    public List<Integer> getOddNumbers() {
        return domain.getOddNumberList();
    }

    /**
     * @return list of even numbers
     */
    @Override
    public List<Integer> getEvenNumbers() {
        return domain.getEvenNumberList();
    }

    /**
     * @return list of reverse order even numbers
     */
    @Override
    public List<Integer> getReverseEvenNumbers() {
        return domain.getReverseEvenNumbers();
    }

    /**
     * @return sum of odd numbers
     */
    @Override
    public int getSumOfOddNumbers() {
        return domain.getSumOfOddNumbers();
    }

    /**
     * @return sum of even numbers
     */
    @Override
    public int getSumOfEvenNumbers() {
        return domain.getSumOfEvenNumbers();
    }

    /**
     * This method is used to get Fibonacci numbers
     * in an interval with value of numbers[amount]
     *
     * @param amount this parameter is the value
     *              of Fibonacci numbers
     *
     * @return list of Fibonacci numbers
     */
    @Override
    public List<Integer> getFibonacciNumber(int amount) {
        return domain.getFibonacciNumber(amount);
    }

    /**
     * This method calculates the percentage of
     * the occurrence of odd numbers
     *
     * @return a percentage of odd Fibonacci numbers
     */
    @Override
    public int getPercentageOfOddFibonacciNumbers() {
        return domain.getPercentageOfOddFibonacciNumbers();
    }

    /**
     * This method calculates the percentage of
     * the occurrence of even numbers
     *
     * @return a percentage of even Fibonacci numbers
     */
    @Override
    public int getPercentageOfEvenFibonacciNumbers() {
        return domain.getPercentageOfEvenFibonacciNumbers();
    }
}
