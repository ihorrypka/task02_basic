package com.epam.fibonacci.task.model;

import java.util.List;

public interface Model {
    List<Integer> getNumberList(int firstNumber, int lastNumber);
    List<Integer> getOddNumbers();
    List<Integer> getEvenNumbers();
    List<Integer> getReverseEvenNumbers();
    int getSumOfOddNumbers();
    int getSumOfEvenNumbers();
    List<Integer> getFibonacciNumber(int amount);
    int getPercentageOfOddFibonacciNumbers();
    int getPercentageOfEvenFibonacciNumbers();
}
