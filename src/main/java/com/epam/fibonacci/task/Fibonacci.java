package com.epam.fibonacci.task;

import com.epam.fibonacci.task.view.MyView;
/**
 * @author Ihor Rypka
 * @version 1.0
 * @since 08.03.2019
 */
public class Fibonacci {
    /**
     * This is the main method which starts the program.
     *
     * @param args Unused.
     */
    public static void main(String[] args) {
        new MyView().show();
    }
}
